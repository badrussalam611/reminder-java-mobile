package com.tbreminder.source.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tbreminder.source.R;
import com.tbreminder.source.object.control;

import java.util.ArrayList;

/**
 * Created by batam on 5/1/2018.
 */

public class controlAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<control> mDataSource;

    public controlAdapter(Context context, ArrayList<control> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = mInflater.inflate(R.layout.list_item_control, parent, false);

        TextView txt_no = (TextView) rowView.findViewById(R.id.txt_no);
        TextView txt_name = (TextView) rowView.findViewById(R.id.txt_name);
        TextView txt_time = (TextView) rowView.findViewById(R.id.txt_time);
        TextView txt_date = (TextView) rowView.findViewById(R.id.txt_date);

        control n_control = (control) getItem(position);
        txt_no.setText(""+(position+1));
        txt_name.setText(n_control.name);
        txt_time.setText(n_control.time);
        txt_date.setText(n_control.date);

        return rowView;
    }

    public static Drawable getDrawable(String name, Context context) {
        int resourceId = context.getResources().getIdentifier(name, "drawable", context.getPackageName());
        return context.getResources().getDrawable(resourceId);
    }
}
