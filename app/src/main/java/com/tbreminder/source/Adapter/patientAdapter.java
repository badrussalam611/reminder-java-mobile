package com.tbreminder.source.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tbreminder.source.R;
import com.tbreminder.source.object.patient;

import java.util.ArrayList;

/**
 * Created by batam on 4/27/2018.
 */

public class patientAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<patient> mDataSource;

    public patientAdapter(Context context, ArrayList<patient> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = mInflater.inflate(R.layout.list_item_patient, parent, false);

        TextView txt_ktp = (TextView) rowView.findViewById(R.id.txt_ktp);
        TextView txt_name = (TextView) rowView.findViewById(R.id.txt_name);
        TextView txt_ttl = (TextView) rowView.findViewById(R.id.txt_ttl);
        TextView txt_address = (TextView) rowView.findViewById(R.id.txt_address);
        TextView txt_phone = (TextView) rowView.findViewById(R.id.txt_phone);

        patient n_patient = (patient) getItem(position);
        txt_ktp.setText(n_patient.id_ktp);
        txt_name.setText(n_patient.name);
        txt_ttl.setText(n_patient.birth_place+", "+n_patient.birth_date);
        txt_address.setText(n_patient.address);
        txt_phone.setText(n_patient.phone);

        return rowView;
    }

    public static Drawable getDrawable(String name, Context context) {
        int resourceId = context.getResources().getIdentifier(name, "drawable", context.getPackageName());
        return context.getResources().getDrawable(resourceId);
    }
}