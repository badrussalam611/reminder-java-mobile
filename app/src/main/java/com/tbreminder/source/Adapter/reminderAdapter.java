package com.tbreminder.source.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tbreminder.source.R;
import com.tbreminder.source.object.reminder;

import java.util.ArrayList;

/**
 * Created by batam on 4/30/2018.
 */

public class reminderAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<reminder> mDataSource;

    public reminderAdapter(Context context, ArrayList<reminder> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = mInflater.inflate(R.layout.list_item_reminder, parent, false);

        TextView txt_name = (TextView) rowView.findViewById(R.id.txt_name);
        TextView txt_time = (TextView) rowView.findViewById(R.id.txt_time);
        TextView txt_type = (TextView) rowView.findViewById(R.id.txt_type);
        TextView txt_date = (TextView) rowView.findViewById(R.id.txt_date);
        TextView txt_date_control = (TextView) rowView.findViewById(R.id.txt_date_control);

        reminder n_reminder = (reminder) getItem(position);
        txt_name.setText(n_reminder.name);
        txt_time.setText(n_reminder.time);
        txt_type.setText(n_reminder.type);
        txt_date.setText(n_reminder.date);
        txt_date_control.setText(n_reminder.date_control);

        return rowView;
    }

    public static Drawable getDrawable(String name, Context context) {
        int resourceId = context.getResources().getIdentifier(name, "drawable", context.getPackageName());
        return context.getResources().getDrawable(resourceId);
    }
}
