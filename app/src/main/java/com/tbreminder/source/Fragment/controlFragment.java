package com.tbreminder.source.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.tbreminder.source.Adapter.controlAdapter;
import com.tbreminder.source.R;
import com.tbreminder.source.module.HomeActivity;
import com.tbreminder.source.object.control;

import java.util.ArrayList;

/**
 * Created by batam on 5/1/2018.
 */

public class controlFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.control_patient_layout, container, false);

        final ArrayList<control> controlList = control.getControlFromFile("control.json", getActivity());
        ListView lv = (ListView)rootView.findViewById(R.id.control_patient_listview);
        lv.setAdapter(new controlAdapter(getActivity(), controlList));

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                control x = controlList.get(position);

                HomeActivity a =  (HomeActivity) getActivity();
                a.openEditControl(x.id_ktp);
            }
        });

        Button btn_add_patient = (Button) rootView.findViewById(R.id.btn_add_control);
        btn_add_patient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HomeActivity a =  (HomeActivity) getActivity();
                a.openAddControl();
            }
        });

        return rootView;
    }
}
