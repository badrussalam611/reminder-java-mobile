package com.tbreminder.source.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.tbreminder.source.Adapter.doctorAdapter;
import com.tbreminder.source.R;
import com.tbreminder.source.module.HomeActivity;
import com.tbreminder.source.object.doctor;

import java.util.ArrayList;

/**
 * Created by batam on 4/29/2018.
 */

public class doctorFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.doctor_layout, container, false);

        final ArrayList<doctor> doctorList = doctor.getDoctorFromFile("doctor.json", getActivity());
        ListView lv = (ListView)rootView.findViewById(R.id.doctor_listview);
        lv.setAdapter(new doctorAdapter(getActivity(), doctorList));

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                doctor x = doctorList.get(position);

                HomeActivity a =  (HomeActivity) getActivity();
                a.openEditDoctor(x.id);
            }
        });

        Button btn_add_doctor = (Button) rootView.findViewById(R.id.btn_add_doctor);
        btn_add_doctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HomeActivity a =  (HomeActivity) getActivity();
                a.openAddDoctor();
            }
        });

        return rootView;
    }
}

