package com.tbreminder.source.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.tbreminder.source.Adapter.reminderAdapter;
import com.tbreminder.source.R;
import com.tbreminder.source.module.HomeActivity;
import com.tbreminder.source.object.reminder;

import java.util.ArrayList;

/**
 * Created by batam on 4/30/2018.
 */

public class reminderFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.reminder_layout, container, false);

        final ArrayList<reminder> reminderList = reminder.getReminderFromFile("reminder.json", getActivity());
        ListView lv = (ListView)rootView.findViewById(R.id.reminder_listview);
        lv.setAdapter(new reminderAdapter(getActivity(), reminderList));

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                reminder x = reminderList.get(position);

                HomeActivity a =  (HomeActivity) getActivity();
                a.openEditReminder(x.id);
            }
        });

        Button btn_add_doctor = (Button) rootView.findViewById(R.id.btn_add_reminder);
        btn_add_doctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HomeActivity a =  (HomeActivity) getActivity();
                a.openAddReminder();
            }
        });

        return rootView;
    }
}
