package com.tbreminder.source.module;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.tbreminder.source.Fragment.TabReminderFragment;
import com.tbreminder.source.Fragment.addControlFragment;
import com.tbreminder.source.Fragment.addDoctorFragment;
import com.tbreminder.source.Fragment.addReminderFragment;
import com.tbreminder.source.Fragment.controlFragment;
import com.tbreminder.source.Fragment.addPatientFragment;
import com.tbreminder.source.Fragment.doctorFragment;
import com.tbreminder.source.Fragment.editControlFragment;
import com.tbreminder.source.Fragment.editDoctorFragment;
import com.tbreminder.source.Fragment.editPatientFragment;
import com.tbreminder.source.Fragment.editReminderFragment;
import com.tbreminder.source.Fragment.homeFragment;
import com.tbreminder.source.Fragment.patientFragment;
import com.tbreminder.source.R;
import com.tbreminder.source.navbar.AppBaseActivity;

/**
 * Created by Chandra on 11/04/2018.
 */

public class HomeActivity extends AppBaseActivity {

    DrawerLayout mDrawerLayout;
    FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mFragmentManager = getSupportFragmentManager();

        openHome();

        ImageView nav_home = (ImageView) findViewById(R.id.nav_home);
        ImageView nav_doctor = (ImageView) findViewById(R.id.nav_doctor);
        ImageView nav_reminder = (ImageView) findViewById(R.id.nav_reminder);
        ImageView nav_control_patient = (ImageView) findViewById(R.id.nav_control_patient);

        nav_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openHome();
            }
        });

        nav_doctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDoctor();
            }
        });

        nav_reminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTabReminder();
            }
        });

        nav_control_patient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openControlPatient();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_menu);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                //Profile

                break;
            case R.id.item2:
                //Draft

                break;
            case R.id.item3:
                //Idendity Patient
                openPatient();

                break;
            case R.id.item4:
                //Help

                break;
            case R.id.item5:
                //Setting

                break;
            case R.id.item6:
                //Logout

                break;
        }
        return false;
        //item.getItemId() = 0;
    }
    public void openHome()
    {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        Bundle bundle = new Bundle();

        homeFragment x = new homeFragment();
        x.setArguments(bundle);
        fragmentTransaction.replace(R.id.containerView, x, "notiftab").commit();

        setTaksbarInfo("Home");
    }

    public void openPatient()
    {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        Bundle bundle = new Bundle();

        patientFragment x = new patientFragment();
        x.setArguments(bundle);
        fragmentTransaction.replace(R.id.containerView, x, "notiftab").commit();

        setTaksbarInfo("Idenditas Pasien");
    }

    public void openDoctor()
    {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        Bundle bundle = new Bundle();

        doctorFragment x = new doctorFragment();
        x.setArguments(bundle);
        fragmentTransaction.replace(R.id.containerView, x, "doctorFragment").commit();

        setTaksbarInfo("Jadwal Dokter");
    }

    public void openControlPatient()
    {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        Bundle bundle = new Bundle();

        controlFragment x = new controlFragment();
        x.setArguments(bundle);
        fragmentTransaction.replace(R.id.containerView, x, "controlpatient").commit();

        setTaksbarInfo("Jadwal Kontrol Pasien");
    }

    public void openEditDoctor(String id_doctor)
    {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putString("id_doctor", id_doctor);
        editDoctorFragment x = new editDoctorFragment();
        x.setArguments(bundle);
        fragmentTransaction.replace(R.id.containerView, x, "editdoctor").commit();

        setTaksbarInfo("Ubah Jadwal Dokter");
    }

    public void openEditReminder(String id_reminder)
    {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putString("id_reminder", id_reminder);
        editReminderFragment x = new editReminderFragment();
        x.setArguments(bundle);
        fragmentTransaction.replace(R.id.containerView, x, "editreminder").commit();

        setTaksbarInfo("Ubah Reminder");
    }

    public void openEditPatient(String id_patient)
    {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putString("id_patient", id_patient);
        editPatientFragment x = new editPatientFragment();
        x.setArguments(bundle);
        fragmentTransaction.replace(R.id.containerView, x, "editpatient").commit();

        setTaksbarInfo("Ubah Data Pasien");
    }

    public void openEditControl(String id_patient)
    {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putString("id_patient", id_patient);
        editControlFragment x = new editControlFragment();
        x.setArguments(bundle);
        fragmentTransaction.replace(R.id.containerView, x, "editcontrol").commit();

        setTaksbarInfo("Ubah Jadwal Kontrol");
    }

    public void openAddControl()
    {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        Bundle bundle = new Bundle();
        addControlFragment x = new addControlFragment();
        x.setArguments(bundle);
        fragmentTransaction.replace(R.id.containerView, x, "addcontrol").commit();

        setTaksbarInfo("Tambah Jadwal Kontrol");
    }

    public void openAddReminder()
    {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        Bundle bundle = new Bundle();
        addReminderFragment x = new addReminderFragment();
        x.setArguments(bundle);
        fragmentTransaction.replace(R.id.containerView, x, "addreminder").commit();

        setTaksbarInfo("Tambah Reminder");
    }

    public void openAddDoctor()
    {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        Bundle bundle = new Bundle();
        addDoctorFragment x = new addDoctorFragment();
        x.setArguments(bundle);
        fragmentTransaction.replace(R.id.containerView, x, "adddoctor").commit();

        setTaksbarInfo("Tambah Jadwal Dokter");
    }

    public void openAddPatient()
    {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        Bundle bundle = new Bundle();
        addPatientFragment x = new addPatientFragment();
        x.setArguments(bundle);
        fragmentTransaction.replace(R.id.containerView, x, "addpatient").commit();

        setTaksbarInfo("Tambah Pasien");
    }

    public void openTabReminder()
    {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        Bundle bundle = new Bundle();

        TabReminderFragment x = new TabReminderFragment();
        x.setArguments(bundle);
        fragmentTransaction.replace(R.id.containerView, x, "tabRFragment").commit();

        setTaksbarInfo("Informasi Reminder");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            //You can get
            Log.d("NAV","OPEN");
            bukaMenu();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setTaksbarInfo(String info)
    {
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(info);
    }
}
