package com.tbreminder.source.object;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by batam on 5/1/2018.
 */

public class control {
    public String id_ktp;
    public String name;
    public String time;
    public String date;

    public static ArrayList<control> getControlFromFile(String filename, Context context){
        final ArrayList<control> controlList = new ArrayList<>();

        try {
            // Load data
            String jsonString = loadJsonFromAsset(filename, context);
            JSONObject json = new JSONObject(jsonString);
            JSONArray controls = json.getJSONArray("controls");

            // Get achieve objects from JSON file
            for(int i = 0; i < controls.length(); i++){
                control n_control = new control();
                n_control.id_ktp = controls.getJSONObject(i).getString("ktp");
                n_control.name = controls.getJSONObject(i).getString("name");
                n_control.time = controls.getJSONObject(i).getString("time");
                n_control.date = controls.getJSONObject(i).getString("date");

                controlList.add(n_control);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return controlList;
    }

    private static String loadJsonFromAsset(String filename, Context context) {
        String json = null;

        try {
            InputStream is = context.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        }
        catch (java.io.IOException ex) {
            ex.printStackTrace();
            return null;
        }

        return json;
    }

    public control()
    {

    }
}
