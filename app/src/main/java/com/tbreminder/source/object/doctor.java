package com.tbreminder.source.object;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by batam on 4/29/2018.
 */

public class doctor {
    public String id;
    public String name;
    public String date;
    public String time;

    public static ArrayList<doctor> getDoctorFromFile(String filename, Context context){
        final ArrayList<doctor> doctorList = new ArrayList<>();

        try {
            // Load data
            String jsonString = loadJsonFromAsset(filename, context);
            JSONObject json = new JSONObject(jsonString);
            JSONArray doctors = json.getJSONArray("doctors");

            // Get achieve objects from JSON file
            for(int i = 0; i < doctors.length(); i++){
                doctor n_doctor = new doctor();
                n_doctor.id = doctors.getJSONObject(i).getString("id");
                n_doctor.name = doctors.getJSONObject(i).getString("name");
                n_doctor.date = doctors.getJSONObject(i).getString("date");
                n_doctor.time = doctors.getJSONObject(i).getString("time");

                doctorList.add(n_doctor);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return doctorList;
    }

    private static String loadJsonFromAsset(String filename, Context context) {
        String json = null;

        try {
            InputStream is = context.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        }
        catch (java.io.IOException ex) {
            ex.printStackTrace();
            return null;
        }

        return json;
    }

    public doctor()
    {

    }
}
