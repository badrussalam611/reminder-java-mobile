package com.tbreminder.source.object;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by batam on 4/27/2018.
 */

public class patient {

    public String id_ktp;
    public String name;
    public String birth_place;
    public String birth_date;
    public String address;
    public String phone;

    public static ArrayList<patient> getPatientFromFile(String filename, Context context){
        final ArrayList<patient> patientList = new ArrayList<>();

        try {
            // Load data
            String jsonString = loadJsonFromAsset(filename, context);
            JSONObject json = new JSONObject(jsonString);
            JSONArray patients = json.getJSONArray("patients");

            // Get achieve objects from JSON file
            for(int i = 0; i < patients.length(); i++){
                patient n_patient = new patient();
                n_patient.id_ktp = patients.getJSONObject(i).getString("ktp");
                n_patient.name = patients.getJSONObject(i).getString("name");
                n_patient.birth_place = patients.getJSONObject(i).getString("birth_place");
                n_patient.birth_date = patients.getJSONObject(i).getString("birth_date");
                n_patient.address = patients.getJSONObject(i).getString("address");
                n_patient.phone = patients.getJSONObject(i).getString("phone");

                patientList.add(n_patient);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return patientList;
    }

    private static String loadJsonFromAsset(String filename, Context context) {
        String json = null;

        try {
            InputStream is = context.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        }
        catch (java.io.IOException ex) {
            ex.printStackTrace();
            return null;
        }

        return json;
    }

    public patient()
    {

    }
}
