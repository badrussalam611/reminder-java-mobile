package com.tbreminder.source.object;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by batam on 4/30/2018.
 */

public class reminder {
    public String id;
    public String name;
    public String time;
    public String type;
    public String date;
    public String date_control;

    public static ArrayList<reminder> getReminderFromFile(String filename, Context context){
        final ArrayList<reminder> reminderList = new ArrayList<>();

        try {
            // Load data
            String jsonString = loadJsonFromAsset(filename, context);
            JSONObject json = new JSONObject(jsonString);
            JSONArray reminders = json.getJSONArray("reminders");

            // Get achieve objects from JSON file
            for(int i = 0; i < reminders.length(); i++){
                reminder n_reminder = new reminder();
                n_reminder.id = reminders.getJSONObject(i).getString("id");
                n_reminder.name = reminders.getJSONObject(i).getString("name");
                n_reminder.time = reminders.getJSONObject(i).getString("time");
                n_reminder.type = reminders.getJSONObject(i).getString("type");
                n_reminder.date = reminders.getJSONObject(i).getString("date");
                n_reminder.date_control = reminders.getJSONObject(i).getString("date_control");

                reminderList.add(n_reminder);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return reminderList;
    }

    private static String loadJsonFromAsset(String filename, Context context) {
        String json = null;

        try {
            InputStream is = context.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        }
        catch (java.io.IOException ex) {
            ex.printStackTrace();
            return null;
        }

        return json;
    }

    public reminder()
    {

    }
}
